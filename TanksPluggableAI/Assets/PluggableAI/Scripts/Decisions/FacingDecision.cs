﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/FacingTarget")]
public class FacingDecision : Decision
{
    public override bool Decide(StateController controller)
    {
        return FacingTarget(controller);
    }

    // Reference: https://gist.github.com/JannickLeismann/bfbb8d0d37c38e78e22e60ae211b597f
    private bool FacingTarget(StateController controller)
    {
        Vector3 forward = controller.transform.forward;
        Vector3 toTarget = (controller.target.transform.position - controller.transform.position).normalized;

        // 1 = looking to the front of the object
        // -1 = looking to the back of the object
        if (Vector3.Dot(forward, toTarget) < 0f)
        {
            //Debug.Log("Not facing...");
            return false;
        }

        return true;
    }
}
