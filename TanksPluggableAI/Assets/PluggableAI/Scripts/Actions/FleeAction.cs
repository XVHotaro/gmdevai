﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/Flee")]
public class FleeAction : Action
{
    public override void Act(StateController controller)
    {
        Flee(controller);
    }

    private void Flee(StateController controller)
    {
        // Get the direction to the target and go to its opposite
        if (controller.target)
        {
            //Debug.Log("Target: " + controller.target.gameObject.name);
            controller.navMeshAgent.isStopped = false;

            Vector3 toTarget = controller.transform.position - controller.target.position;
            //controller.navMeshAgent.destination = toTarget.normalized * 20f;

                controller.navMeshAgent.destination = controller.transform.position + toTarget;
        }
    }
}
